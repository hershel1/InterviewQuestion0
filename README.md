# InterviewQuestion0

Interview Question for Prellis Biologics:
Requirements:
* Python 3
* Use pipenv

Instructions:
This coding challenge should take about 40-60 minutes.

Given an image, the goal of this exercise is to make the image into a binary image,
split the image into m x n subimages under a certain pixel count. If the total
number of pixels in the subimage exceeds the pixel count, several subimages(of the same size)
should be created where the total number of pixel in each image is smaller than the limit.
Finally, display the image in a certain order (specified in the code).
An optional portion is provided to show your creativity.
