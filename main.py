import cv2
import sys

def load_image_greyscale(image):
    """This function should return a binary image. The thresholding function
        should be explained as your design choice.
    """
    pass

def split_image_m_n_subimages(image, m, n, pixel_count):
    """This function should split the image into m x n subimages with no more
        than pixel_count white pixels in subimage. If there are more, the subimage
        should be replicated and the pixel count split so each image will be below the
        threshold but the sum of the images will show the overall image.
    """
    pass

def display_images(image_set):
    """Given a image_set, this function should display each image in a linear raster order
        i.e. [1 2 3; 4 5 6; 7 8 9] would display 1 2 3 4 5 6 7 8 9.
    """
    pass

def surprise_me():
    """OPTIONAL: Write a function to do something surprising :)
    Maybe a different display order or some kind of parallelism/optimization.
    """
    pass

if __name__ == '__main__':
    image_path = sys.argv[1]
    grey_image = load_image_greyscale(image_path)
    image_set = split_image_m_n_subimages(grey_image)
    display_images(image_set)

    # OPTIONAL AND ADD WHERE YOU WOULD LIKE
    # surprise_me()
